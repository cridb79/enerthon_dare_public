"""Module to read PlanungsdatenAnlagen."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from const import DATA_DIR, PROJECT_ROOT
from datetime import datetime, timedelta
from typing import Tuple


from planning_data_reader import planning_data
from redispatch_data_reader import read_redispatch_plan, read_redispatch

dfs_red_plans, dicts_red_plans = read_redispatch_plan()
dfs_red, dicts_red = read_redispatch()

# redispatch 1
i = 0
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{6}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch1_plan, c="grey", linewidth=5, label="Redispatch 1 plan"
    )
    plt.plot(df.Plan + df.Redispatch1, c="cyan", linewidth=5, label="Redispatch 1")
    plt.plot(
        df.Plan + df.Redispatch1.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch1_plan, c="grey", linewidth=5, label="Redispatch 1 plan"
    )
    plt.plot(df.Plan - df.Redispatch1, c="cyan", linewidth=5, label="Redispatch 1")
    plt.plot(
        df.Plan - df.Redispatch1.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch1_internal.png")
plt.show()

# redispatch 2
i = 1
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{2}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch2_plan, c="grey", linewidth=5, label="Redispatch 2 plan"
    )
    plt.plot(df.Plan + df.Redispatch2, c="cyan", linewidth=5, label="Redispatch 2")
    plt.plot(
        df.Plan + df.Redispatch2.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch2_plan, c="grey", linewidth=5, label="Redispatch 2 plan"
    )
    plt.plot(df.Plan - df.Redispatch2, c="cyan", linewidth=5, label="Redispatch 2")
    plt.plot(
        df.Plan - df.Redispatch2.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch2_internal.png")
plt.show()

# redispatch 3
i = 2
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{3}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch3_plan, c="grey", linewidth=5, label="Redispatch 3 plan"
    )
    plt.plot(df.Plan + df.Redispatch3, c="cyan", linewidth=5, label="Redispatch 3")
    plt.plot(
        df.Plan + df.Redispatch3.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch3_plan, c="grey", linewidth=5, label="Redispatch 3 plan"
    )
    plt.plot(df.Plan - df.Redispatch3, c="cyan", linewidth=5, label="Redispatch 3")
    plt.plot(
        df.Plan - df.Redispatch3.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch3_internal.png")
plt.show()

# redispatch 6
i = 5
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{8}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch6_plan, c="grey", linewidth=5, label="Redispatch 6 plan"
    )
    plt.plot(df.Plan + df.Redispatch6, c="cyan", linewidth=5, label="Redispatch 6")
    plt.plot(
        df.Plan + df.Redispatch6.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch6_plan, c="grey", linewidth=5, label="Redispatch 6 plan"
    )
    plt.plot(df.Plan - df.Redispatch6, c="cyan", linewidth=5, label="Redispatch 6")
    plt.plot(
        df.Plan - df.Redispatch6.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch6_internal.png")
plt.show()

# redispatch 7
i = 6
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{12}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch7_plan, c="grey", linewidth=5, label="Redispatch 7 plan"
    )
    plt.plot(df.Plan + df.Redispatch7, c="cyan", linewidth=5, label="Redispatch 7")
    plt.plot(
        df.Plan + df.Redispatch7.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch7_plan, c="grey", linewidth=5, label="Redispatch 7 plan"
    )
    plt.plot(df.Plan - df.Redispatch7, c="cyan", linewidth=5, label="Redispatch 7")
    plt.plot(
        df.Plan - df.Redispatch7.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch7_internal.png")
plt.show()

# redispatch 8
i = 7
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{14}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch8_plan, c="grey", linewidth=5, label="Redispatch 8 plan"
    )
    plt.plot(df.Plan + df.Redispatch8, c="cyan", linewidth=5, label="Redispatch 8")
    plt.plot(
        df.Plan + df.Redispatch8.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch8_plan, c="grey", linewidth=5, label="Redispatch 8 plan"
    )
    plt.plot(df.Plan - df.Redispatch8, c="cyan", linewidth=5, label="Redispatch 8")
    plt.plot(
        df.Plan - df.Redispatch8.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch8_internal.png")
plt.show()

# redispatch 9
i = 8
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{16}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch9_plan, c="grey", linewidth=5, label="Redispatch 9 plan"
    )
    plt.plot(df.Plan + df.Redispatch9, c="cyan", linewidth=5, label="Redispatch 9")
    plt.plot(
        df.Plan + df.Redispatch9.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch9_plan, c="grey", linewidth=5, label="Redispatch 9 plan"
    )
    plt.plot(df.Plan - df.Redispatch9, c="cyan", linewidth=5, label="Redispatch 9")
    plt.plot(
        df.Plan - df.Redispatch9.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch9_internal.png")
plt.show()

# redispatch 10
i = 9
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{16}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch10_plan,
        c="grey",
        linewidth=5,
        label="Redispatch 10 plan",
    )
    plt.plot(df.Plan + df.Redispatch10, c="cyan", linewidth=5, label="Redispatch 10")
    plt.plot(
        df.Plan + df.Redispatch10.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch10_plan,
        c="grey",
        linewidth=5,
        label="Redispatch 10 plan",
    )
    plt.plot(df.Plan - df.Redispatch10, c="cyan", linewidth=5, label="Redispatch 10")
    plt.plot(
        df.Plan - df.Redispatch10.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch10_internal.png")
plt.show()
