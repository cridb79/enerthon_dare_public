"""Dashboard powered by Plotly Dash."""

import dash
import dash_auth
from dash import dcc
from dash import html
from dash import dash_table
from dash.dependencies import Input, Output, State
import dash_daq as daq


import plotly.express as px

import pandas as pd
import numpy as np

from enerthon_dare.planning_data_reader import planning_data, master_data
from enerthon_dare.redispatch_data_reader import (
    aggregate_redispatch,
    read_redispatch_plan,
    read_redispatch,
)
from enerthon_dare.const import VALID_USERNAME_PASSWORD_PAIRS

olt_color_dict = {
    "dark_blue": "#236192",
    "light_blue": "#9BB8D3",
    "red": "#A50034",
    "dark_orange": "#D0103A",
    "orange": "#ED8B00",
    "purple": "#8E3A80",
    "dark_green": "#0D776E",
    "turquoise": "#009AA6",
    "light_green": "#91D8AE",
    "blue75": "#407899",
    "red75": "#a41944",
    "dark_orange75": "#dc4e3a",
    "orange_75": "#e87d2b",
    "blue50": "#7fa5bb",
    "red50": "#b85261",
    "dark_orange50": "#e4785d",
    "orange_50": "#ee9a56",
    "blue25": "#bfd2dd",
    "red25": "#ce8288",
    "dark_orange25": "#eda184",
    "orange_25": "#f3ba83",
    "blue15": "#d9e4eb",
    "red15": "#e4bbb6",
    "dark_orange15": "#f6ccb7",
    "orange_15": "#f9d9b7",
}


def create_app():
    """Dash/flask app factory."""

    external_stylesheets = [
        "./assets/style.css",
        "https://codepen.io/chriddyp/pen/bWLwgP.css",
    ]

    app = dash.Dash(
        __name__,
        meta_tags=[{"name": "viewport", "content": "width=device-width"}],
        external_stylesheets=external_stylesheets,
    )

    auth = dash_auth.BasicAuth(app, VALID_USERNAME_PASSWORD_PAIRS)

    app.config.suppress_callback_exceptions = True

    app.title = "Enerthon - DARE"

    df = master_data()

    betreiber_dict = {
        i: col
        for i, col in zip(
            df["Anschlussnetzbetreiber"].unique(),
            ["#236192", "#9BB8D3", "#A50034", "#D0103A", "#8E3A80"],
        )
    }

    df["prov_color"] = df["Anschlussnetzbetreiber"].apply(lambda x: betreiber_dict[x])

    df["power"] = np.log2(
        df["Nettonennleistung (TRs)"].apply(
            lambda x: float(x.split(" ")[0].replace(".", "").replace(",", "."))
        )
    )
    df["power"] /= df["power"].mean()

    df["type"] = df["Klarname"].apply(lambda x: x[:-3])
    type_dict = {
        i: sym for i, sym in zip(df["type"].unique(), ["#0D776E", "#009AA6", "#91D8AE"])
    }
    df["type_color"] = df["type"].apply(lambda x: type_dict[x])

    df_redispatches = aggregate_redispatch()
    df_redispatches.reset_index(inplace=True)
    redisp_dict = {True: "Redispatch", False: "Standard"}

    app.layout = html.Div(
        [
            html.Div(
                [
                    html.Img(
                        src="https://www.dare-plattform.de/file/2021/09/Logo-ohne-Claim.jpg"
                    ),
                    html.H1("Enerthon - DA / RE"),
                    html.Img(src="./assets/OLT_Logo_horizontal_RGB_M.png"),
                ],
                id="header",
            ),
            html.Div(
                [
                    dcc.Graph(
                        id="MapPlot",
                    ),
                ],
                style={
                    "border-bottom": "1px solid #636363",
                },
                id="map-container",
            ),
            html.Div(
                [
                    dcc.RadioItems(
                        options=[
                            {
                                "label": "Anschlussnetzbetreiber",
                                "value": "Anschlussnetzbetreiber",
                            },
                            {"label": "Type", "value": "type"},
                            {"label": "Redispatch", "value": "redispatch"},
                        ],
                        value="Anschlussnetzbetreiber",
                        labelStyle={"display": "inline-block", "margin-right": "15px"},
                        style={"display": "inline-flex", "margin-left": "0"},
                        id="radio-colors",
                    ),
                    daq.BooleanSwitch(
                        on=False,
                        color="#7cbe82",
                        label="  Advanced View",
                        labelPosition="left",
                        id="advanced-switch",
                        style={"display": "inline-flex", "margin-right": "0"},
                    ),
                ],
                style={
                    "margin-top": "10px",
                    "margin-left": "10px",
                    "display": "flex",
                    "flex-wrap": "wrap",
                    "justify-content": "space-around",
                    "align-items": "center",
                    "width": "100%",
                },
            ),
            html.Div(
                [
                    dcc.Interval(
                        id="animate", interval=250, max_intervals=480, disabled=True
                    ),
                    dcc.Slider(
                        min=0,
                        max=480,
                        value=0,
                        step=1,
                        marks={
                            int(i): df_redispatches.loc[int(i), "Timestamp"].strftime(
                                "%m-%d %H:%M"
                            )
                            for i in np.arange(0, 480, 48)
                        },
                        id="time-slider",
                    ),
                    html.Button(
                        "Play / Stop",
                        id="play",
                        style={
                            "display": "inline-block",
                            "margin-top": "10px",
                        },
                    ),
                    html.P(
                        id="time-text",
                        style={"display": "inline-block", "margin-left": "20px"},
                    ),
                ],
                id="time-slider-div",
                style={
                    "margin-top": "25px",
                    "margin-left": "50px",
                    "margin-right": "50px",
                    "margin-bottom": "15px",
                    "display": "none",
                },
            ),
            html.Div(
                [
                    html.Div(
                        [
                            dcc.Graph(
                                id="redispatch-chart",
                            ),
                        ],
                        style={
                            "margin": "auto",
                            "width": "60%",
                            "display": "flex-block",
                        },
                    ),
                    html.Div(
                        [
                            dash_table.DataTable(
                                id="table",
                                style_cell={"textAlign": "left"},
                                style_as_list_view=True,
                            ),
                        ],
                        style={
                            "width": "25%",
                            "display": "flex-block",
                            "margin": "auto",
                            "align-items": "center",
                            "justify-content": "flex-start",
                            "padding-bottom": "30px",
                        },
                    ),
                ],
                id="advanced-panel",
                style={
                    "display": "none",
                },
            ),
        ]
    )

    @app.callback(
        [Output("table", "data"), Output("table", "columns")],
        Input("MapPlot", "clickData"),
    )
    def update_table(selected_powerPlant):
        # Get the power plant ID
        if selected_powerPlant is not None:
            df_table = df[df["lon"] == selected_powerPlant["points"][0]["lon"]]
            df_table.drop(
                columns=["lat", "lon", "prov_color", "power", "type", "type_color"],
                inplace=True,
            )
            df_table = df_table.T.reset_index()
            df_table = df_table.rename(columns={"Unnamed: 0": ""})
            columns = [{"name": i, "id": i} for i in df_table.columns]
            data = df_table.to_dict("records")

            return data, columns

    def find(lst, key, value):
        for i, dic in enumerate(lst):
            if dic[key] == value:
                return i
        return -1

    @app.callback(Output("redispatch-chart", "figure"), Input("MapPlot", "clickData"))
    def update_plot(selected_powerPlant):
        """Update the plot showing the time series data after clicking on a power plant in the map."""

        # Get the power plant ID
        if selected_powerPlant is not None:
            ppIndex = df.index[df["lon"] == selected_powerPlant["points"][0]["lon"]][
                0
            ].split()[-1]
            print(ppIndex)
        if "ppIndex" not in locals():
            ppIndex = "1"

        # Read Data
        df_plan, dicts_plan = planning_data(f"PlanungsdatenAnlage" + ppIndex)
        dfs_red_plans, dicts_red_plans = read_redispatch_plan()
        dfs_red, dicts_red = read_redispatch()

        # Match data based on "Resource Object" and "MP_ID"
        index_red = find(dicts_red, "Resource Object", dicts_plan["Resource Object"])

        # Plot if there is no need to redispatch
        if index_red == -1:
            df_plan["date"] = df_plan.index
            if "Red_plus" in df_plan.columns:
                df_plan["Positive Redispatch Capacity"] = (
                    df_plan["Plan"] + df_plan["Red_plus"]
                )
                df_plan.drop(columns=["Red_plus"], inplace=True)
            if "Red_minus" in df_plan.columns:
                df_plan["Negative Redispatch Capacity"] = (
                    df_plan["Plan"] - df_plan["Red_minus"]
                )
                df_plan.drop(columns=["Red_minus"], inplace=True)

            figure = px.line(
                df_plan,
                x="date",
                y=df_plan.columns,
                title="Power Plant " + ppIndex,
                color_discrete_sequence=[
                    "black",
                    olt_color_dict["dark_blue"],
                    olt_color_dict["light_blue"],
                ],
            )

        # Plot if there has been a redispatch
        else:
            df_plan = df_plan.merge(
                dfs_red[index_red], how="outer", left_index=True, right_index=True
            )
            df_plan = df_plan.merge(
                dfs_red_plans[index_red], how="outer", left_index=True, right_index=True
            )
            df_plan["Operation"] = df_plan["Redispatch" + str(index_red + 1)]
            df_plan.Operation.fillna(df_plan.Plan, inplace=True)
            df_plan["Operation_plan"] = df_plan[
                "Redispatch" + str(index_red + 1) + "_plan"
            ]
            df_plan.Operation_plan.fillna(df_plan.Plan, inplace=True)
            df_plan["Operation"] = df_plan["Operation"].astype(object)
            df_plan["Operation_plan"] = df_plan["Operation_plan"].astype(object)

            if "Red_plus" in df_plan.columns:
                df_plan["Positive Redispatch Capacity"] = (
                    df_plan["Plan"] + df_plan["Red_plus"]
                )
                df_plan.drop(columns=["Red_plus"], inplace=True)
            if "Red_minus" in df_plan.columns:
                df_plan["Negative Redispatch Capacity"] = (
                    df_plan["Plan"] - df_plan["Red_minus"]
                )
                df_plan.drop(columns=["Red_minus"], inplace=True)

            if dicts_red[index_red]["Direction"] in ["A01"]:
                df_plan["Redispatch Plan"] = (
                    df_plan["Plan"]
                    + df_plan["Redispatch" + str(index_red + 1) + "_plan"]
                )
            elif dicts_red[index_red]["Direction"] in ["A02"]:
                df_plan["Redispatch Plan"] = (
                    df_plan["Plan"]
                    - df_plan["Redispatch" + str(index_red + 1) + "_plan"]
                )

            df_plan = df_plan.drop(
                columns=["Redispatch" + str(index_red + 1) + "_plan", "Operation_plan"]
            )

            # Produce actual figure
            figure = px.line(
                df_plan,
                x=df_plan.index,
                y=df_plan.columns,
                color_discrete_sequence=[
                    "black",
                    olt_color_dict["dark_blue"],
                    olt_color_dict["light_blue"],
                    olt_color_dict["red50"],
                    olt_color_dict["dark_orange"],
                ],
                title="Power Plant " + ppIndex,
            )

        figure.update_layout(
            {
                "plot_bgcolor": "rgba(0, 0, 0, 0)",
                "paper_bgcolor": "rgba(0, 0, 0, 0)",
                "title_font_color": "#636363",
                "legend_title": "Legend",
            }
        )
        figure.update_xaxes(
            zeroline=True,
            zerolinewidth=1,
            zerolinecolor="#636363",
            gridcolor="lightgray",
            title_text="",
            title_font=dict(size=18, family="sans-serif", color="#636363"),
        )
        figure.update_yaxes(
            zeroline=True,
            zerolinewidth=1,
            gridcolor="lightgray",
            zerolinecolor="#636363",
            title_text="Redispatch",
            title_font=dict(size=18, family="sans-serif", color="#636363"),
        )
        return figure

    @app.callback(
        [Output("MapPlot", "figure"), Output("time-slider-div", "style")],
        [
            Input("radio-colors", "value"),
            Input("time-slider", "value"),
            Input("animate", "n_intervals"),
        ],
    )
    def update_map(selected_colors, step, n):
        mapbox_access_token = "pk.eyJ1IjoiYXR1cmNhdGkiLCJhIjoiY2t2am1mMDhzMDcxYTJ4b3Uycm5sYXhyNSJ9.rHuHIbU3-dfbY-hlxvK7ig"

        map_layout = dict(
            autosize=True,
            # height=510,
            automargin=True,
            margin=dict(l=0, r=0, b=0, t=0),
            hovermode="closest",
            plot_bgcolor="rgba(0,0,0,0)",
            paper_bgcolor="rgba(0,0,0,0)",
            legend=dict(font=dict(size=10), orientation="h"),
            title=None,
            mapbox=dict(
                accesstoken=mapbox_access_token,
                style="light",
                center=dict(lon=df.lon.mean(), lat=df.lat.mean() - 0.3),
                zoom=6,
            ),
        )
        if selected_colors != "redispatch":
            slider_style = {"margin-top": "10px", "display": "none"}

            if selected_colors == "Anschlussnetzbetreiber":
                col_dict = betreiber_dict
            elif selected_colors == "type":
                col_dict = type_dict

            traces = []
            for type, dfff in df.groupby(selected_colors):
                trace = {
                    "type": "scattermapbox",
                    "lat": list(dfff.lat),
                    "lon": list(dfff.lon),
                    "hoverinfo": "text",
                    "hovertext": [
                        f"Name: {i} | Type: {j} | Anschlussnetzbetreiber: {k} | Nettonennleistung: {l}"
                        for i, j, k, l in zip(
                            dfff.index.to_list(),
                            dfff["type"],
                            dfff["Anschlussnetzbetreiber"],
                            dfff["Nettonennleistung (TRs)"],
                        )
                    ],
                    "mode": "markers",
                    "name": type,
                    "showlegend": True,
                    "marker": {
                        "size": 10 * dfff["power"],
                        "opacity": 0.7,
                        "color": col_dict[type],
                    },
                }
                traces.append(trace)

            figure = dict(data=traces, layout=map_layout)

        else:
            if n is not None:
                next_step = step + int(n)
                if (next_step > 0) and (next_step < 480):
                    step = next_step
            redisp = df_redispatches.drop(columns=["Timestamp"]).iloc[step].to_list()
            redisp = [redisp_dict[i > 0] for i in redisp]
            df["redispatch"] = redisp

            slider_style = {
                "margin-top": "10px",
                "margin-left": "50px",
                "margin-right": "50px",
                "display": "block",
            }
            col_dict = {"Redispatch": "#7cbe82", "Standard": "#636363"}
            size_dict = {"Redispatch": 16, "Standard": 8}

            traces = []
            for type, dfff in df.groupby("redispatch"):
                trace = {
                    "type": "scattermapbox",
                    "lat": list(dfff.lat),
                    "lon": list(dfff.lon),
                    "hoverinfo": "text",
                    "hovertext": [
                        f"Name: {i} | Type: {j} | Anschlussnetzbetreiber: {k} | Nettonennleistung: {l}"
                        for i, j, k, l in zip(
                            dfff.index.to_list(),
                            dfff["type"],
                            dfff["Anschlussnetzbetreiber"],
                            dfff["Nettonennleistung (TRs)"],
                        )
                    ],
                    "mode": "markers",
                    "name": type,
                    "showlegend": True,
                    "marker": {
                        "size": size_dict[type] * dfff["power"],
                        "opacity": 0.9,
                        "color": col_dict[type],
                    },
                }
                traces.append(trace)

            figure = dict(data=traces, layout=map_layout)

        return figure, slider_style

    @app.callback(
        Output("animate", "disabled"),
        Input("play", "n_clicks"),
        State("animate", "disabled"),
    )
    def toggle(n, playing):
        if n:
            return not playing
        return playing

    @app.callback(Output("time-slider", "value"), Input("animate", "n_intervals"))
    def update_slider(n):
        if n is not None:
            return n
        else:
            return 0

    @app.callback(Output("animate", "n_intervals"), Input("time-slider", "value"))
    def update_interval(n):
        if n is not None:
            return n
        else:
            return 0

    @app.callback(Output("advanced-panel", "style"), Input("advanced-switch", "on"))
    def show_advanced_panel(on):
        if on:
            return {
                "display": "block",
                "margin-top": "25px",
                "background-color": "rgb(240,240,239)",
                "border-top": "1px solid #636363",
            }
        else:
            return {"display": "none"}

    @app.callback(Output("time-text", "children"), Input("time-slider", "value"))
    def test_date(step):
        return f"Time: {df_redispatches.loc[int(step), 'Timestamp'].strftime('%m-%d %H:%M')}"

    return app.server
