"""Module to read PlanungsdatenAnlagen."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from const import DATA_DIR, PROJECT_ROOT
from datetime import datetime, timedelta
from typing import Tuple


from planning_data_reader import planning_data
from redispatch_data_reader import read_redispatch_plan, read_redispatch

dfs_red_plans, dicts_red_plans = read_redispatch_plan()
dfs_red, dicts_red = read_redispatch()

# redispatch 1
i = 0
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{6}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(df.Plan + df.Redispatch1, c="cyan", linewidth=5, label="Redispatch 1")
    plt.plot(
        df.Plan + df.Redispatch1.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(df.Plan - df.Redispatch1, c="cyan", linewidth=5, label="Redispatch 1")
    plt.plot(
        df.Plan - df.Redispatch1.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch1.png")
plt.show()

# redispatch 2
i = 1
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{2}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(df.Plan + df.Redispatch2, c="cyan", linewidth=5, label="Redispatch 2")
    plt.plot(
        df.Plan + df.Redispatch2.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(df.Plan - df.Redispatch2, c="cyan", linewidth=5, label="Redispatch 2")
    plt.plot(
        df.Plan - df.Redispatch2.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch2.png")
plt.show()

# redispatch 3
i = 2
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{3}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(df.Plan + df.Redispatch3, c="cyan", linewidth=5, label="Redispatch 3")
    plt.plot(
        df.Plan + df.Redispatch3.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(df.Plan - df.Redispatch3, c="cyan", linewidth=5, label="Redispatch 3")
    plt.plot(
        df.Plan - df.Redispatch3.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch3.png")
plt.show()

# redispatch 6
i = 5
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{8}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(df.Plan + df.Redispatch6, c="cyan", linewidth=5, label="Redispatch 6")
    plt.plot(
        df.Plan + df.Redispatch6.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(df.Plan - df.Redispatch6, c="cyan", linewidth=5, label="Redispatch 6")
    plt.plot(
        df.Plan - df.Redispatch6.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch6.png")
plt.show()

# redispatch 7
i = 6
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{12}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(df.Plan + df.Redispatch7, c="cyan", linewidth=5, label="Redispatch 7")
    plt.plot(
        df.Plan + df.Redispatch7.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(df.Plan - df.Redispatch7, c="cyan", linewidth=5, label="Redispatch 7")
    plt.plot(
        df.Plan - df.Redispatch7.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch7.png")
plt.show()

# redispatch 8
i = 7
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{14}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(df.Plan + df.Redispatch8, c="cyan", linewidth=5, label="Redispatch 8")
    plt.plot(
        df.Plan + df.Redispatch8.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(df.Plan - df.Redispatch8, c="cyan", linewidth=5, label="Redispatch 8")
    plt.plot(
        df.Plan - df.Redispatch8.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch8.png")
plt.show()

# redispatch 9
i = 8
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{16}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(df.Plan + df.Redispatch9, c="cyan", linewidth=5, label="Redispatch 9")
    plt.plot(
        df.Plan + df.Redispatch9.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(df.Plan - df.Redispatch9, c="cyan", linewidth=5, label="Redispatch 9")
    plt.plot(
        df.Plan - df.Redispatch9.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch9.png")
plt.show()

# redispatch 10
i = 9
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{16}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(df.Plan + df.Red_plus, label="Positive redispatch capacity")
if "Red_minus" in df.columns:
    plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(df.Plan + df.Redispatch10, c="cyan", linewidth=5, label="Redispatch 10")
    plt.plot(
        df.Plan + df.Redispatch10.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(df.Plan - df.Redispatch10, c="cyan", linewidth=5, label="Redispatch 10")
    plt.plot(
        df.Plan - df.Redispatch10.fillna(0),
        c="magenta",
        linestyle="--",
        label="Operation",
    )
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch10.png")
plt.show()

# redispatches 4 and 5 (special)
i = 3
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{1}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red[i + 1], how="outer", left_index=True, right_index=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Plan - df.Red_minus, label="Negative redispatch capacity")
plt.plot(df.Plan - df.Redispatch4, c="cyan", linewidth=5, label="Redispatch 4")
plt.plot(df.Plan - df.Redispatch5, c="green", linewidth=5, label="Redispatch 5")
plt.plot(
    df.Plan - df.Redispatch4.fillna(0) - df.Redispatch5.fillna(0),
    c="magenta",
    linestyle="--",
    label="Operation",
)
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch4_5.png")
plt.show()
