"""Module to read PlanungsdatenAnlagen."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from const import DATA_DIR, PROJECT_ROOT
from datetime import datetime, timedelta
from typing import Tuple


from planning_data_reader import planning_data
from redispatch_data_reader import read_redispatch_plan, read_redispatch

dfs_red_plans, dicts_red_plans = read_redispatch_plan()
dfs_red, dicts_red = read_redispatch()

# redispatch 1
i = 0
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{6}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch1"]
df.Operation.fillna(df.Plan, inplace=True)
df["Operation_plan"] = df["Redispatch1_plan"]
df.Operation_plan.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(
        df.Plan + df.Red_plus,
        c="green",
        linestyle="--",
        label="Positive redispatch capacity",
    )
if "Red_minus" in df.columns:
    plt.plot(
        df.Plan - df.Red_minus,
        c="red",
        linestyle="--",
        label="Negative redispatch capacity",
    )
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch1_plan, c="green", linewidth=5, label="Redispatch 1 plan"
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch1_plan, c="green", linewidth=5, label="Redispatch 1 plan"
    )
plt.plot(df.Redispatch1, linewidth=5, label="Redispatch 1")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch1_internal.png")
plt.show()
plt.close("all")

# redispatch 2
i = 1
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{2}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch2"]
df.Operation.fillna(df.Plan, inplace=True)
df["Operation_plan"] = df["Redispatch2_plan"]
df.Operation_plan.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(
        df.Plan + df.Red_plus,
        c="green",
        linestyle="--",
        label="Positive redispatch capacity",
    )
if "Red_minus" in df.columns:
    plt.plot(
        df.Plan - df.Red_minus,
        c="red",
        linestyle="--",
        label="Negative redispatch capacity",
    )
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch2_plan, c="green", linewidth=5, label="Redispatch 2 plan"
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch2_plan, c="green", linewidth=5, label="Redispatch 2 plan"
    )
plt.plot(df.Redispatch2, linewidth=5, label="Redispatch 2")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch2_internal.png")
plt.show()
plt.close("all")

# redispatch 3
i = 2
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{3}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch3"]
df.Operation.fillna(df.Plan, inplace=True)
df["Operation_plan"] = df["Redispatch3_plan"]
df.Operation_plan.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(
        df.Plan + df.Red_plus,
        c="green",
        linestyle="--",
        label="Positive redispatch capacity",
    )
if "Red_minus" in df.columns:
    plt.plot(
        df.Plan - df.Red_minus,
        c="red",
        linestyle="--",
        label="Negative redispatch capacity",
    )
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch3_plan, c="green", linewidth=5, label="Redispatch 3 plan"
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch3_plan, c="green", linewidth=5, label="Redispatch 3 plan"
    )
plt.plot(df.Redispatch3, linewidth=5, label="Redispatch 3")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch3_internal.png")
plt.show()
plt.close("all")

# redispatch 6
i = 5
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{8}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch6"]
df.Operation.fillna(df.Plan, inplace=True)
df["Operation_plan"] = df["Redispatch6_plan"]
df.Operation_plan.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(
        df.Plan + df.Red_plus,
        c="green",
        linestyle="--",
        label="Positive redispatch capacity",
    )
if "Red_minus" in df.columns:
    plt.plot(
        df.Plan - df.Red_minus,
        c="red",
        linestyle="--",
        label="Negative redispatch capacity",
    )
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch6_plan, c="green", linewidth=5, label="Redispatch 6 plan"
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch6_plan, c="green", linewidth=5, label="Redispatch 6 plan"
    )
plt.plot(df.Redispatch6, linewidth=5, label="Redispatch 6")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch6_internal.png")
plt.show()
plt.close("all")

# redispatch 7
i = 6
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{12}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch7"]
df.Operation.fillna(df.Plan, inplace=True)
df["Operation_plan"] = df["Redispatch7_plan"]
df.Operation_plan.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(
        df.Plan + df.Red_plus,
        c="green",
        linestyle="--",
        label="Positive redispatch capacity",
    )
if "Red_minus" in df.columns:
    plt.plot(
        df.Plan - df.Red_minus,
        c="red",
        linestyle="--",
        label="Negative redispatch capacity",
    )
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch7_plan, c="green", linewidth=5, label="Redispatch 7 plan"
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch7_plan, c="green", linewidth=5, label="Redispatch 7 plan"
    )
plt.plot(df.Redispatch7, linewidth=5, label="Redispatch 7")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch7_internal.png")
plt.show()
plt.close("all")

# redispatch 8
i = 7
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{14}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch8"]
df.Operation.fillna(df.Plan, inplace=True)
df["Operation_plan"] = df["Redispatch8_plan"]
df.Operation_plan.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(
        df.Plan + df.Red_plus,
        c="green",
        linestyle="--",
        label="Positive redispatch capacity",
    )
if "Red_minus" in df.columns:
    plt.plot(
        df.Plan - df.Red_minus,
        c="red",
        linestyle="--",
        label="Negative redispatch capacity",
    )
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch8_plan, c="green", linewidth=5, label="Redispatch 8 plan"
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch8_plan, c="green", linewidth=5, label="Redispatch 8 plan"
    )
plt.plot(df.Redispatch8, linewidth=5, label="Redispatch 8")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch8_internal.png")
plt.show()
plt.close("all")

# redispatch 9
i = 8
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{16}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch9"]
df.Operation.fillna(df.Plan, inplace=True)
df["Operation_plan"] = df["Redispatch9_plan"]
df.Operation_plan.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(
        df.Plan + df.Red_plus,
        c="green",
        linestyle="--",
        label="Positive redispatch capacity",
    )
if "Red_minus" in df.columns:
    plt.plot(
        df.Plan - df.Red_minus,
        c="red",
        linestyle="--",
        label="Negative redispatch capacity",
    )
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch9_plan, c="green", linewidth=5, label="Redispatch 9 plan"
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch9_plan, c="green", linewidth=5, label="Redispatch 9 plan"
    )
plt.plot(df.Redispatch9, linewidth=5, label="Redispatch 9")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch9_internal.png")
plt.show()
plt.close("all")

# redispatch 10
i = 9
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{16}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch10"]
df.Operation.fillna(df.Plan, inplace=True)
df["Operation_plan"] = df["Redispatch10_plan"]
df.Operation_plan.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(
        df.Plan + df.Red_plus,
        c="green",
        linestyle="--",
        label="Positive redispatch capacity",
    )
if "Red_minus" in df.columns:
    plt.plot(
        df.Plan - df.Red_minus,
        c="red",
        linestyle="--",
        label="Negative redispatch capacity",
    )
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch10_plan,
        c="green",
        linewidth=5,
        label="Redispatch 10 plan",
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch10_plan,
        c="green",
        linewidth=5,
        label="Redispatch 10 plan",
    )
plt.plot(df.Redispatch10, linewidth=5, label="Redispatch 10")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch10_internal.png")
plt.show()
plt.close("all")

# redispatches 4 and 5 (special)
i = 3
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{1}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red[i + 1], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red_plans[i + 1], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch4"]
df.Operation.fillna(df.Redispatch5, inplace=True)
df.Operation.fillna(df.Plan, inplace=True)
df["Operation_plan"] = df["Redispatch4_plan"]
df.Operation_plan.fillna(df.Redispatch5_plan, inplace=True)
df.Operation_plan.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
if "Red_plus" in df.columns:
    plt.plot(
        df.Plan + df.Red_plus,
        c="green",
        linestyle="--",
        label="Positive redispatch capacity",
    )
if "Red_minus" in df.columns:
    plt.plot(
        df.Plan - df.Red_minus,
        c="red",
        linestyle="--",
        label="Negative redispatch capacity",
    )
if dicts_red[i]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch4_plan, c="green", linewidth=5, label="Redispatch 4 plan"
    )
elif dicts_red[i]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch4_plan, c="green", linewidth=5, label="Redispatch 4 plan"
    )
if dicts_red[i + 1]["Direction"] in ["A01"]:
    plt.plot(
        df.Plan + df.Redispatch5_plan,
        c="yellow",
        linewidth=5,
        label="Redispatch 5 plan",
    )
elif dicts_red[i + 1]["Direction"] in ["A02"]:
    plt.plot(
        df.Plan - df.Redispatch5_plan,
        c="yellow",
        linewidth=5,
        label="Redispatch 5 plan",
    )
plt.plot(df.Redispatch4, linewidth=5, label="Redispatch4")
plt.plot(df.Redispatch5, linewidth=5, c="cyan", label="Redispatch5")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch4_5_internal.png")
plt.show()
plt.close("all")
