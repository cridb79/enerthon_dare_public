"""Module to read PlanungsdatenAnlagen."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from const import DATA_DIR, PROJECT_ROOT
from datetime import datetime, timedelta
from typing import Tuple


from planning_data_reader import planning_data
from redispatch_data_reader import read_redispatch_plan, read_redispatch

df, _ = planning_data(f"PlanungsdatenAnlage{1}")
dfs_red_plans, dicts_red_plans = read_redispatch_plan()
dfs_red, dicts_red = read_redispatch()

cols = [
    "PP01",
    "PP02",
    "PP03",
    "PP04",
    "PP05",
    "PP06",
    "PP07",
    "PP08",
    "PP09",
    "PP10",
    "PP11",
    "PP12",
    "PP13",
    "PP14",
    "PP15",
    "PP16",
    "PP17",
    "PP18",
]

master_df = pd.DataFrame(0, index=df.index, columns=cols)

for i in np.arange(9):
    pp = dicts_red[i]["Resource Object"][-2:]
    col = f"PP{pp}"
    red_ind = dfs_red[i].dropna().index
    master_df[col][master_df.index.isin(red_ind)] = 1
