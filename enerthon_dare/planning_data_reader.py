"""Module to read PlanungsdatenAnlagen."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from .const import DATA_DIR, PROJECT_ROOT
from datetime import datetime, timedelta
from typing import Tuple


def datetime_range(start, end, delta):
    current = start
    while current < end:
        yield current
        current += delta


def master_data() -> pd.DataFrame:
    df = pd.read_excel(
        f"{DATA_DIR}/Testdata Enerthon 21.xlsx",
        sheet_name="Master Data",
        engine="openpyxl",
    ).T
    df.columns = df.loc["Unnamed: 0"]
    df = df.dropna(axis=1).drop("Unnamed: 0")
    df["lat"] = df["Geokoordinaten TRs"].apply(
        lambda x: float(x.split(" ")[0].split("=")[1].replace(",", "."))
    )
    df["lon"] = df["Geokoordinaten TRs"].apply(
        lambda x: float(x.split(" ")[1].split("=")[1].replace(",", "."))
    )
    return df


def planning_data(sheet_name: str) -> Tuple[pd.DataFrame, dict]:
    """[summary]

    Args:
        sheet_name (str): [description]

    Returns:
        Tuple[pd.DataFrame, dict]: [description]
    """
    df = pd.read_excel(
        f"{DATA_DIR}/Testdata Enerthon 21.xlsx",
        sheet_name=sheet_name,
        engine="openpyxl",
        skiprows=0,
    )

    df.set_index("Unnamed: 0", inplace=True)

    klarname = df.loc["Klarname"][0]
    senderidentification = df.loc["SenderIdentification"][0]
    resourceobject = df.loc["Resource Object"][0]
    timeinterval = df.loc["TimeInterval (UTC)"]
    businesstype = df.loc["BusinessType"]
    direction = df.loc["Direction"]

    df = df[7:]
    dts = [
        dt.strftime("%Y-%m-%d %H:%M")
        for dt in datetime_range(
            datetime(2021, 6, 2, 0, 0),
            datetime(2021, 6, 3, 0, 0),
            timedelta(minutes=15),
        )
    ]
    df["Timestamp"] = dts
    df.set_index("Timestamp", inplace=True)
    df.index = pd.to_datetime(df.index)

    # plan
    df_d1 = pd.DataFrame(df["Day1"]).rename({"Day1": "Plan"}, axis="columns")
    df_d2 = pd.DataFrame(df["Day2"]).rename({"Day2": "Plan"}, axis="columns")
    df_d2.index = df_d2.index + pd.Timedelta(days=1)
    df_d3 = pd.DataFrame(df["Day3"]).rename({"Day3": "Plan"}, axis="columns")
    df_d3.index = df_d3.index + pd.Timedelta(days=2)
    df_d4 = pd.DataFrame(df["Day4"]).rename({"Day4": "Plan"}, axis="columns")
    df_d4.index = df_d4.index + pd.Timedelta(days=3)
    df_d5 = pd.DataFrame(df["Day5"]).rename({"Day5": "Plan"}, axis="columns")
    df_d5.index = df_d5.index + pd.Timedelta(days=4)
    df_plan = df_d1.append(df_d2).append(df_d3).append(df_d4).append(df_d5)

    # redispatch 1
    df_d1_1 = pd.DataFrame(df["Day1.1"]).rename({"Day1.1": "Red"}, axis="columns")
    df_d2_1 = pd.DataFrame(df["Day2.1"]).rename({"Day2.1": "Red"}, axis="columns")
    df_d2_1.index = df_d2_1.index + pd.Timedelta(days=1)
    df_d3_1 = pd.DataFrame(df["Day3.1"]).rename({"Day3.1": "Red"}, axis="columns")
    df_d3_1.index = df_d3_1.index + pd.Timedelta(days=2)
    df_d4_1 = pd.DataFrame(df["Day4.1"]).rename({"Day4.1": "Red"}, axis="columns")
    df_d4_1.index = df_d4_1.index + pd.Timedelta(days=3)
    df_d5_1 = pd.DataFrame(df["Day5.1"]).rename({"Day5.1": "Red"}, axis="columns")
    df_d5_1.index = df_d5_1.index + pd.Timedelta(days=4)

    if direction[6] in ["A01", "A01 "]:
        df_red_plus = (
            df_d1_1.append(df_d2_1).append(df_d3_1).append(df_d4_1).append(df_d5_1)
        )
        df_red_plus.rename({"Red": "Red_plus"}, axis="columns", inplace=True)
        df_merged = df_plan.merge(df_red_plus, left_index=True, right_index=True)
    elif direction[6] in ["A02", "A02 "]:
        df_red_minus = (
            df_d1_1.append(df_d2_1).append(df_d3_1).append(df_d4_1).append(df_d5_1)
        )
        df_red_minus.rename({"Red": "Red_minus"}, axis="columns", inplace=True)
        df_merged = df_plan.merge(df_red_minus, left_index=True, right_index=True)

    # redispatch 2
    if len(timeinterval) > 10:
        df_d1_2 = pd.DataFrame(df["Day1.2"]).rename({"Day1.2": "Red"}, axis="columns")
        df_d2_2 = pd.DataFrame(df["Day2.2"]).rename({"Day2.2": "Red"}, axis="columns")
        df_d2_2.index = df_d2_2.index + pd.Timedelta(days=1)
        df_d3_2 = pd.DataFrame(df["Day3.2"]).rename({"Day3.2": "Red"}, axis="columns")
        df_d3_2.index = df_d3_2.index + pd.Timedelta(days=2)
        df_d4_2 = pd.DataFrame(df["Day4.2"]).rename({"Day4.2": "Red"}, axis="columns")
        df_d4_2.index = df_d4_2.index + pd.Timedelta(days=3)
        df_d5_2 = pd.DataFrame(df["Day5.2"]).rename({"Day5.2": "Red"}, axis="columns")
        df_d5_2.index = df_d5_2.index + pd.Timedelta(days=4)

        if direction[11] in ["A01", "A01 "]:
            df_red_plus = (
                df_d1_2.append(df_d2_2).append(df_d3_2).append(df_d4_2).append(df_d5_2)
            )
            df_red_plus.rename({"Red": "Red_plus"}, axis="columns", inplace=True)
            df_merged = df_merged.merge(df_red_plus, left_index=True, right_index=True)
        elif direction[11] in ["A02", "A02 "]:
            df_red_minus = (
                df_d1_2.append(df_d2_2).append(df_d3_2).append(df_d4_2).append(df_d5_2)
            )
            df_red_minus.rename({"Red": "Red_minus"}, axis="columns", inplace=True)
            df_merged = df_merged.merge(df_red_minus, left_index=True, right_index=True)

    info = {
        "Klarname": klarname,
        "SenderIdentification": senderidentification,
        "Resource Object": resourceobject,
    }

    return df_merged, info


if __name__ == "__main__":

    for i in np.arange(18) + 1:
        df, _ = planning_data(f"PlanungsdatenAnlage{i}")
        print(_, df)
        plt.figure(figsize=(16, 9))
        plt.plot(df.Plan, label="Plan")
        if "Red_plus" in df.columns:
            plt.plot(
                df.Plan + df.Red_plus,
                c="green",
                linestyle="--",
                label="Positive redispatch capacity",
            )
        if "Red_minus" in df.columns:
            plt.plot(
                df.Plan - df.Red_minus,
                c="red",
                linestyle="--",
                label="Negative redispatch capacity",
            )
        plt.legend()
        plt.title(_["Klarname"])
        plt.savefig(f"{PROJECT_ROOT}/private/PlanungsdatenAnlage{i}.png")
        # plt.show()
