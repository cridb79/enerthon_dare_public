"""Module to read PlanungsdatenAnlagen."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from const import DATA_DIR, PROJECT_ROOT
from datetime import datetime, timedelta
from typing import Tuple


from planning_data_reader import planning_data
from redispatch_data_reader import read_redispatch_plan, read_redispatch

dfs_red_plans, dicts_red_plans = read_redispatch_plan()
dfs_red, dicts_red = read_redispatch()

# redispatch 1
i = 0
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{6}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch1"]
df.Operation.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Redispatch1, linewidth=5, label="Redispatch 1")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch1.png")
plt.show()
plt.close("all")

# redispatch 2
i = 1
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{2}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch2"]
df.Operation.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Redispatch2, linewidth=5, label="Redispatch 2")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch2.png")
plt.show()
plt.close("all")

# redispatch 3
i = 2
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{3}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch3"]
df.Operation.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Redispatch3, linewidth=5, label="Redispatch 3")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch3.png")
plt.show()
plt.close("all")

# redispatch 6
i = 5
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{8}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch6"]
df.Operation.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Redispatch6, linewidth=5, label="Redispatch 6")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch6.png")
plt.show()
plt.close("all")

# redispatch 7
i = 6
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{12}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch7"]
df.Operation.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Redispatch7, linewidth=5, label="Redispatch 7")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch7.png")
plt.show()
plt.close("all")

# redispatch 8
i = 7
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{14}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch8"]
df.Operation.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Redispatch8, linewidth=5, label="Redispatch 8")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch8.png")
plt.show()
plt.close("all")

# redispatch 9
i = 8
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{16}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch9"]
df.Operation.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Redispatch9, linewidth=5, label="Redispatch 9")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch9.png")
plt.show()
plt.close("all")

# redispatch 10
i = 9
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{16}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch10"]
df.Operation.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Redispatch10, linewidth=5, label="Redispatch 10")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch10.png")
plt.show()
plt.close("all")

# redispatches 4 and 5 (special)
i = 3
dicts_red[i]["Resource Object"]
df, _ = planning_data(f"PlanungsdatenAnlage{1}")
df = df.merge(dfs_red[i], how="outer", left_index=True, right_index=True)
df = df.merge(dfs_red[i + 1], how="outer", left_index=True, right_index=True)
df["Operation"] = df["Redispatch4"]
df.Operation.fillna(df.Redispatch5, inplace=True)
df.Operation.fillna(df.Plan, inplace=True)

plt.figure(figsize=(16, 9))
plt.plot(df.Plan, label="Plan")
plt.plot(df.Redispatch4, linewidth=5, label="Redispatch 4")
plt.plot(df.Redispatch5, linewidth=5, c="cyan", label="Redispatch 5")
plt.plot(df.Operation, c="magenta", linestyle="--", label="Operation")
plt.legend()
plt.title(_["Klarname"])
plt.savefig(f"{PROJECT_ROOT}/private/Data_w_redispatch4_5.png")
plt.show()
plt.close("all")
