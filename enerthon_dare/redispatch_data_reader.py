"""Module to read PlanungsdatenAnlagen."""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from .const import DATA_DIR, PROJECT_ROOT
from datetime import datetime, timedelta
from typing import Tuple

from enerthon_dare.planning_data_reader import planning_data


def datetime_range(start, end, delta):
    current = start
    while current < end:
        yield current
        current += delta


def transform_df(df, start, end, delta):
    dts = [
        dt.strftime("%Y-%m-%d %H:%M")
        for dt in datetime_range(
            start,
            end,
            delta,
        )
    ]
    df.loc[:, "Timestamp"] = dts
    df.set_index("Timestamp", inplace=True)
    df.index = pd.to_datetime(df.index)

    return df


def redispatch_plans_df(df, start, end, delta, suffix):
    dict = {
        "RD-Bedarf": df.iloc[0].values[0],
        "Klarname NB": df.iloc[1].values[0],
        "Resource Provider": df.iloc[2].values[0],
        "Resource Object": df.iloc[3].values[0],
        "DocumentType": df.iloc[4].values[0],
        "Direction": df.iloc[6].values[0],
    }
    df = df[7:]
    df = transform_df(
        df,
        start,
        end,
        delta,
    ).rename({df.columns[0]: f"Redispatch{suffix}_plan"}, axis="columns")

    return df, dict


def redispatch_df(df, start, end, delta, suffix):
    dict = {
        "Zugehöriger RD-Bedarf": df.iloc[0].values[0],
        "Klarname NB": df.iloc[1].values[0],
        "SenderIIdentification": df.iloc[2].values[0],
        "ReceiverIdentification": df.iloc[3].values[0],
        "Resource Object": df.iloc[4].values[0],
        "BusinessType": df.iloc[5].values[0],
        "Direction": df.iloc[7].values[0],
    }
    df = df[8:]
    df = transform_df(
        df,
        start,
        end,
        delta,
    ).rename({df.columns[0]: f"Redispatch{suffix}"}, axis="columns")

    return df, dict


def read_redispatch_plan():

    df = pd.read_excel(
        f"{DATA_DIR}/Testdata Enerthon 21.xlsx",
        sheet_name="RD-Bedarfe",
        engine="openpyxl",
        skiprows=2,
    )

    df.set_index("Unnamed: 0", inplace=True)

    # redispatch 1
    df1 = df[df.columns[df.iloc[0] == 1]]
    df1, dict1 = redispatch_plans_df(
        df1,
        datetime(2021, 6, 6, 0, 0),
        datetime(2021, 6, 7, 0, 0),
        timedelta(minutes=15),
        suffix=1,
    )
    # redispatch 2
    df2 = df[df.columns[df.iloc[0] == 2]]
    df2, dict2 = redispatch_plans_df(
        df2,
        datetime(2021, 6, 6, 0, 0),
        datetime(2021, 6, 7, 0, 0),
        timedelta(minutes=15),
        suffix=2,
    )
    # redispatch 3
    df3 = df[df.columns[df.iloc[0] == 3]]
    df3, dict3 = redispatch_plans_df(
        df3,
        datetime(2021, 6, 3, 0, 0),
        datetime(2021, 6, 4, 0, 0),
        timedelta(minutes=15),
        suffix=3,
    )
    # redispatch 4
    df4 = df[df.columns[df.iloc[0] == 4]]
    df4, dict4 = redispatch_plans_df(
        df4,
        datetime(2021, 6, 2, 0, 0),
        datetime(2021, 6, 3, 0, 0),
        timedelta(minutes=15),
        suffix=4,
    )
    # redispatch 5
    df5 = df[df.columns[df.iloc[0] == 5]]
    df5, dict5 = redispatch_plans_df(
        df5,
        datetime(2021, 6, 4, 0, 0),
        datetime(2021, 6, 5, 0, 0),
        timedelta(minutes=15),
        suffix=5,
    )
    # redispatch 6
    df6 = df[df.columns[df.iloc[0] == 6]]
    df6, dict6 = redispatch_plans_df(
        df6,
        datetime(2021, 6, 3, 0, 0),
        datetime(2021, 6, 4, 0, 0),
        timedelta(minutes=15),
        suffix=6,
    )
    # redispatch 7
    df7 = df[df.columns[df.iloc[0] == 7]]
    df7, dict7 = redispatch_plans_df(
        df7,
        datetime(2021, 6, 4, 0, 0),
        datetime(2021, 6, 5, 0, 0),
        timedelta(minutes=15),
        suffix=7,
    )
    # redispatch 8
    df8 = df[df.columns[df.iloc[0] == 8]]
    df8, dict8 = redispatch_plans_df(
        df8,
        datetime(2021, 6, 5, 0, 0),
        datetime(2021, 6, 6, 0, 0),
        timedelta(minutes=15),
        suffix=8,
    )
    # redispatch 9
    df9 = df[df.columns[df.iloc[0] == 9]]
    df9, dict9 = redispatch_plans_df(
        df9,
        datetime(2021, 6, 6, 0, 0),
        datetime(2021, 6, 7, 0, 0),
        timedelta(minutes=15),
        suffix=9,
    )
    # redispatch 10
    df10 = df[df.columns[df.iloc[0] == 10]]
    df10, dict10 = redispatch_plans_df(
        df10,
        datetime(2021, 6, 6, 0, 0),
        datetime(2021, 6, 7, 0, 0),
        timedelta(minutes=15),
        suffix=10,
    )

    dfs = [df1, df2, df3, df4, df5, df6, df7, df8, df9, df10]
    dicts = [dict1, dict2, dict3, dict4, dict5, dict6, dict7, dict8, dict9, dict10]

    return dfs, dicts


def read_redispatch():

    df = pd.read_excel(
        f"{DATA_DIR}/Testdata Enerthon 21.xlsx",
        sheet_name="Aktivierungsdokumente",
        engine="openpyxl",
        skiprows=2,
    )

    df.set_index("Unnamed: 0", inplace=True)

    # redispatch 1
    df1 = df[df.columns[df.iloc[0] == 1]]
    df1, dict1 = redispatch_df(
        df1,
        datetime(2021, 6, 6, 0, 0),
        datetime(2021, 6, 7, 0, 0),
        timedelta(minutes=15),
        suffix=1,
    )
    # redispatch 2
    df2 = df[df.columns[df.iloc[0] == 2]]
    df2, dict2 = redispatch_df(
        df2,
        datetime(2021, 6, 6, 0, 0),
        datetime(2021, 6, 7, 0, 0),
        timedelta(minutes=15),
        suffix=2,
    )
    # redispatch 3
    df3 = df[df.columns[df.iloc[0] == 3]]
    df3, dict3 = redispatch_df(
        df3,
        datetime(2021, 6, 3, 0, 0),
        datetime(2021, 6, 4, 0, 0),
        timedelta(minutes=15),
        suffix=3,
    )
    # redispatch 4
    df4 = df[df.columns[df.iloc[0] == 4]]
    df4, dict4 = redispatch_df(
        df4,
        datetime(2021, 6, 2, 0, 0),
        datetime(2021, 6, 3, 0, 0),
        timedelta(minutes=15),
        suffix=4,
    )
    # redispatch 5
    df5 = df[df.columns[df.iloc[0] == 5]]
    df5, dict5 = redispatch_df(
        df5,
        datetime(2021, 6, 4, 0, 0),
        datetime(2021, 6, 5, 0, 0),
        timedelta(minutes=15),
        suffix=5,
    )
    # redispatch 6
    df6 = df[df.columns[df.iloc[0] == 6]]
    df6, dict6 = redispatch_df(
        df6,
        datetime(2021, 6, 3, 0, 0),
        datetime(2021, 6, 4, 0, 0),
        timedelta(minutes=15),
        suffix=6,
    )
    # redispatch 7
    df7 = df[df.columns[df.iloc[0] == 7]]
    df7, dict7 = redispatch_df(
        df7,
        datetime(2021, 6, 4, 0, 0),
        datetime(2021, 6, 5, 0, 0),
        timedelta(minutes=15),
        suffix=7,
    )
    # redispatch 8
    df8 = df[df.columns[df.iloc[0] == 8]]
    df8, dict8 = redispatch_df(
        df8,
        datetime(2021, 6, 5, 0, 0),
        datetime(2021, 6, 6, 0, 0),
        timedelta(minutes=15),
        suffix=8,
    )
    # redispatch 9
    df9 = df[df.columns[df.iloc[0] == 9]]
    df9, dict9 = redispatch_df(
        df9,
        datetime(2021, 6, 6, 0, 0),
        datetime(2021, 6, 7, 0, 0),
        timedelta(minutes=15),
        suffix=9,
    )
    # redispatch 10
    df10 = df[df.columns[df.iloc[0] == 10]]
    df10, dict10 = redispatch_df(
        df10,
        datetime(2021, 6, 6, 0, 0),
        datetime(2021, 6, 7, 0, 0),
        timedelta(minutes=15),
        suffix=10,
    )

    dfs = [df1, df2, df3, df4, df5, df6, df7, df8, df9, df10]
    dicts = [dict1, dict2, dict3, dict4, dict5, dict6, dict7, dict8, dict9, dict10]

    return dfs, dicts


def aggregate_redispatch() -> pd.DataFrame:
    df, _ = planning_data(f"PlanungsdatenAnlage{1}")
    dfs_red_plans, dicts_red_plans = read_redispatch_plan()
    dfs_red, dicts_red = read_redispatch()

    cols = [f"PP{i:02d}" for i in range(1, 19)]

    master_df = pd.DataFrame(0, index=df.index, columns=cols)

    for i in np.arange(9):
        pp = dicts_red[i]["Resource Object"][-2:]
        col = f"PP{pp}"
        red_ind = dfs_red[i].dropna().index
        master_df[col][master_df.index.isin(red_ind)] = 1

    master_df.columns = [f"Power Plant {i}" for i in range(1, 19)]

    return master_df


if __name__ == "__main__":

    dfs_red_plans, dicts_red_plans = read_redispatch_plan()
    dfs_red, dicts_red = read_redispatch()

    for i in np.arange(10):
        plt.figure(figsize=(16, 9))
        if dicts_red[i]["Direction"] in ["A01"]:
            plt.plot(dfs_red_plans[i], label="redispatch plan")
            plt.plot(dfs_red[i], label="redispatch")
        elif dicts_red[i]["Direction"] in ["A02"]:
            plt.plot(-dfs_red_plans[i], label="redispatch plan")
            plt.plot(-dfs_red[i], label="redispatch")
        plt.legend()
        plt.title(
            f"{dicts_red[i]['Zugehöriger RD-Bedarf']} {dicts_red[i]['Klarname NB']} {dicts_red[i]['Resource Object']} {dicts_red[i]['Direction']}"
        )
        plt.savefig(f"{PROJECT_ROOT}/private/Redispatch{i+1}.png")
        # plt.show()

        plt.figure(figsize=(16, 9))
        plt.plot(dfs_red_plans[i], label="redispatch plan")
        plt.plot(dfs_red[i], label="redispatch")
        plt.legend()
        plt.title(
            f"{dicts_red[i]['Zugehöriger RD-Bedarf']} {dicts_red[i]['Klarname NB']} {dicts_red[i]['Resource Object']} {dicts_red[i]['Direction']}"
        )
        plt.savefig(f"{PROJECT_ROOT}/private/Redispatch{i+1}_alt.png")
        # plt.show()
        plt.close("all")
