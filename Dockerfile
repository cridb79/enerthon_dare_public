FROM python:3.8-buster

RUN pip install pipenv

# uncomment below if you need to install .deb dependencies
# RUN apt-get update && apt-get install -y \
#     some-apt-package another-apt-package
#     && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY enerthon_dare/ /app/enerthon_dare/
COPY data/ /app/data/
COPY Pipfile .env /app/

RUN PIPENV_VENV_IN_PROJECT=true pipenv install

EXPOSE $PORT

CMD pipenv run gunicorn -b 0.0.0.0:$PORT enerthon_dare.dashboard.wsgi:app

