# enerthon_dare


Git repo for DARE challenge


## Setup for local development

To set this project up for local development after cloning it from `git`, run

```bash
./scripts/setup
```

This will
- set up a virtual environment and install all dependencies with `pipenv`

## Run the dashboard in a local environment

```bash
pipenv run python -m enerthon_dare.dashboard
```

This will
- run the dashboard on http://127.0.0.1:5000/

## With Docker

1. Build the image
```
pipenv run docker-compose build
```
2. Run the image (optionally add ` -d` to keep the container running in the background)
```
pipenv run docker-compose up
